<?php

namespace App\Services\Plan;

use App\Models\Group;
use App\Services\Plan\Contracts\PlanActionsInterface;

class PlanActions implements PlanActionsInterface
{
  public function changePlan($request, $id)
  {
    $result = Group::find($id)->lectures()->sync($request->lectures);
    return $result;
  }
}
