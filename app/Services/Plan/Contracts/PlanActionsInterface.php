<?php

namespace App\Services\Plan\Contracts;

use App\Http\Requests\Group\PlanChangeRequest;

interface PlanActionsInterface
{
  public function changePlan(PlanChangeRequest $request, $id);
}
