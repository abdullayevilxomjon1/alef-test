<?php

namespace App\Services\Lecture\Contracts;

interface LectureActionsInterface
{
  public function list($id);
}
