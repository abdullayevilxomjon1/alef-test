<?php

namespace App\Services\Lecture;

use App\Http\Resources\Group\GroupsResource;
use App\Http\Resources\Lecture\LectureResource;
use App\Http\Resources\Student\StudentsResource;
use App\Models\Lecture;
use App\Models\Student;
use App\Services\Lecture\Contracts\LectureActionsInterface;

class LectureActions implements LectureActionsInterface
{
  public function list($id)
  {
    $lecture = Lecture::find($id);
    $groups = GroupsResource::collection($lecture->groups);
    $group_ids = $lecture->groups->pluck('id');
    $students = StudentsResource::collection(Student::whereIn('group_id', optional($group_ids)->toArray())->get());
    return [
      'id' => $lecture->id,
      'name' => $lecture->name,
      'description' => $lecture->description,
      'groups' => $groups,
      'students' => $students
    ];
  }
}
