<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Student\StudentCreateRequest;
use App\Http\Requests\Student\StudentRequest;
use App\Http\Requests\Student\StudentUpdateRequest;
use App\Http\Resources\Student\StudentResource;
use App\Http\Resources\Student\StudentsResource;
use App\Models\Student;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    use ApiResponse;

    public function index(Request $request)
    {
        $result = StudentsResource::collection(Student::all());
        return $this->sendResponse($result);
    }

    public function show(StudentRequest $request, $id)
    {
        $result = new StudentResource(Student::find($id));
        return $this->sendResponse($result);
    }

    public function create(StudentCreateRequest $request)
    {
        $result = Student::create($request->all());
        return $this->sendResponse($result, null, 201);
    }

    public function update(StudentUpdateRequest $request, $id)
    {
        $result = Student::where('id', $id)->update($request->all());
        return $this->sendResponse($result);
    }

    public function delete(StudentRequest $request, $id)
    {
        $result = Student::destroy($id);
        return $this->sendResponse($result);
    }
}
