<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Lecture\LectureCreateRequest;
use App\Http\Requests\Lecture\LectureRequest;
use App\Http\Requests\Lecture\LectureUpdateRequest;
use App\Http\Resources\Lecture\LecturesResource;
use App\Models\Lecture;
use App\Services\Lecture\Contracts\LectureActionsInterface;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;

class LectureController extends Controller
{
    use ApiResponse;

    public function index(Request $request)
    {
        $result = LecturesResource::collection(Lecture::all());
        return $this->sendResponse($result);
    }

    public function show(LectureRequest $request, $id, LectureActionsInterface $actions)
    {
        $result = $actions->list($id);
        return $this->sendResponse($result);
    }

    public function create(LectureCreateRequest $request)
    {
        $result = Lecture::create($request->all());
        return $this->sendResponse($result);
    }

    public function update(LectureUpdateRequest $request, $id)
    {
        $result = Lecture::where('id', $id)->update($request->all());
        return $this->sendResponse($result);
    }

    public function delete(LectureRequest $request, $id)
    {
        $result = Lecture::destroy($id);
        return $this->sendResponse($result);
    }
}
