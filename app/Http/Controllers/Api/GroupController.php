<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Group\GroupCreateRequest;
use App\Http\Requests\Group\GroupRequest;
use App\Http\Requests\Group\GroupUpdateRequest;
use App\Http\Requests\Group\PlanChangeRequest;
use App\Http\Resources\Group\GroupPlanResource;
use App\Http\Resources\Group\GroupResource;
use App\Http\Resources\Group\GroupsResource;
use App\Models\Group;
use App\Services\Plan\Contracts\PlanActionsInterface;
use App\Traits\ApiResponse;

class GroupController extends Controller
{
    use ApiResponse;

    public function index()
    {
        $result = GroupsResource::collection(Group::all());
        return $this->sendResponse($result);
    }

    public function show(GroupRequest $request, $id)
    {
        $result = new GroupResource(Group::find($id));
        return $this->sendResponse($result);
    }

    public function create(GroupCreateRequest $request)
    {
        $result = Group::create($request->all());
        return $this->sendResponse($result, null, 201);
    }

    public function update(GroupUpdateRequest $request, $id)
    {
        $result = Group::where('id', $id)->update($request->all());
        return $this->sendResponse($result);
    }

    public function delete(GroupRequest $request, $id)
    {
        $result = Group::destroy($id);
        return $this->sendResponse($result);
    }

    public function plan(GroupRequest $request, $id)
    {
        $result = new GroupPlanResource(Group::find($id));
        return $this->sendResponse($result);
    }

    public function changePlan(PlanChangeRequest $request, $id, PlanActionsInterface $actions)
    {
        $result = $actions->changePlan($request, $id);
        return $this->sendResponse($result);
    }
}
