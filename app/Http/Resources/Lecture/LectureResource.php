<?php

namespace App\Http\Resources\Lecture;

use Illuminate\Http\Resources\Json\JsonResource;

class LectureResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->lecture->id,
            'name' => $this->lecture->name,
            'description' => $this->lecture->description,
        ];
    }
}
