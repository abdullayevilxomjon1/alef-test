<?php

namespace App\Http\Resources\Group;

use App\Http\Resources\Lecture\LecturesResource;
use Illuminate\Http\Resources\Json\JsonResource;

class GroupPlanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'plan' => LecturesResource::collection($this->lectures)
        ];
    }
}
