<?php

namespace App\Http\Resources\Student;

use App\Http\Resources\Group\GroupsResource;
use App\Http\Resources\Lecture\LecturesResource;
use Illuminate\Http\Resources\Json\JsonResource;

class StudentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'group' => new GroupsResource($this->group),
            'lectures' => LecturesResource::collection(optional($this->group)->lectures ?? [])
        ];
    }
}
