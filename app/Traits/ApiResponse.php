<?php

namespace App\Traits;

trait ApiResponse
{
  /**
   * success response method.
   *
   * @return \Illuminate\Http\Response
   */
  public function sendResponse($result, $message = null, $code = 200)
  {
    $response = [
      'success' => true,
      'message' => $message,
      'data'    => $result,
    ];

    return response()->json($response, $code);
  }

  /**
   * return error response.
   *
   * @return \Illuminate\Http\Response
   */
  public function sendError($error, $errorMessages = [], $code = 404)
  {
    $response = [
      'success' => false,
      'message' => $error,
    ];

    if (!empty($errorMessages)) {
      $response['data'] = $errorMessages;
    }
    return response()->json($response, $code);
  }
}
