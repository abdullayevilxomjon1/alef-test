<?php

namespace App\Providers;

use App\Services\Lecture\Contracts\LectureActionsInterface;
use App\Services\Lecture\LectureActions;
use App\Services\Plan\Contracts\PlanActionsInterface;
use App\Services\Plan\PlanActions;
use Illuminate\Support\ServiceProvider;

class ApiServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PlanActionsInterface::class, PlanActions::class);
        $this->app->bind(LectureActionsInterface::class, LectureActions::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
