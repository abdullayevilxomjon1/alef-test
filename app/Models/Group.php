<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasFactory;
    protected $fillable = ['name'];

    public function lectures()
    {
        return $this->belongsToMany(Lecture::class, 'plans');
    }

    public function students()
    {
        return $this->hasMany(Student::class);
    }

    public function plan()
    {
        return $this->hasOne(Plan::class);
    }
}
