<?php

use App\Http\Controllers\Api\GroupController;
use App\Http\Controllers\Api\LectureController;
use App\Http\Controllers\Api\StudentController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'student'], function () {
    Route::get('', [StudentController::class, 'index']);
    Route::get('{id}', [StudentController::class, 'show']);
    Route::post('', [StudentController::class, 'create']);
    Route::put('{id}', [StudentController::class, 'update']);
    Route::delete('{id}', [StudentController::class, 'delete']);
});

Route::group(['prefix' => 'group'], function () {
    Route::get('', [GroupController::class, 'index']);
    Route::get('{id}', [GroupController::class, 'show']);
    Route::post('', [GroupController::class, 'create']);
    Route::put('{id}', [GroupController::class, 'update']);
    Route::delete('{id}', [GroupController::class, 'delete']);
    Route::get('{id}/plan', [GroupController::class, 'plan']);
    Route::put('{id}/plan', [GroupController::class, 'changePlan']);
});

Route::group(['prefix' => 'lecture'], function () {
    Route::get('', [LectureController::class, 'index']);
    Route::get('{id}', [LectureController::class, 'show']);
    Route::post('', [LectureController::class, 'create']);
    Route::put('{id}', [LectureController::class, 'update']);
    Route::delete('{id}', [LectureController::class, 'delete']);
});
