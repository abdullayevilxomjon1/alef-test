<?php

namespace Database\Factories;

use App\Models\Lecture;
use Illuminate\Database\Eloquent\Factories\Factory;

class PlanFactory extends Factory
{
    // protected $model = Plan::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'lecture_id' => Lecture::all()->random()->id,
            'order' => rand(1, 5)
        ];
    }
}
